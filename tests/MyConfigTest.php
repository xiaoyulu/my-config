<?php
declare(strict_types=1);

namespace LiLei\Configs\Tests;

use PHPUnit\Framework\TestCase;
use LiLei\Configs\MyConfig;

class MyConfigTest extends TestCase
{
    // vendor/bin/phpunit tests/MyConfigTest.php --filter testGetConfig
    public function testGetConfig()
    {
        $database = MyConfig::getConfig('database');
        var_dump($database);

        $host = MyConfig::getConfig('database.host');
        var_dump($host);

        $username = MyConfig::getConfig('database.username', null, 'config');
        var_dump($username);
    }// testRun() end
}