# MyConfig

简单易用的配置文件加载类，只支持 PHP 文件。例如：

```php
return [
    'drive'     => 'mysql',
    'host'      => "127.0.0.1",
    'username'  => "root",
    'password'  => "123456",
    'port'      => 3306,
    'prefix'    => "",
];
```



## 使用

```php
use LiLei\Configs\MyConfig;

// 获取的是 project_root/config/database.php 的配置。
MyConfig::getConfig('database.host');

// 获取的是 project_root/app/config/database.php 的配置。
MyConfig::getConfig('database.username', null, 'app/config');
```

